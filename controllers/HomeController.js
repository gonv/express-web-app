const mongoose = require('mongoose');
const Registration = mongoose.model('Registration');
const {validationResult} = require('express-validator');


function HomeController() {
    this.getRoot = (req, res) => {
        res.render('form', { title: 'Registration form'});
    }
    this.postRoot = (req, res) => {
        console.log(req.body);
        const errors = validationResult(req);
        
        //console.log(errors);
        if (!errors.errors.length){
            const registration = new Registration(req.body);
            registration.save()
              .then (() => { res.send('Thank you for your registration!'); })
              .catch((err) => {
                console.log(err);
                res.send('Sorry! Something was wrong!'); 
              });
        }
        else {
            res.render('form', { 
                title: 'Registration form',
                errors: errors.array(),
                data: req.body
            });
        }
    }
}



module.exports = HomeController;