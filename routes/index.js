const express=require('express');
const {check} = require('express-validator');

const HomeController = new require('../controllers/HomeController');
const RegistrationController = new require('../controllers/RegistrationController');

const homeController = new HomeController();
const registrationController = new RegistrationController();

const router=express.Router();

router.get('/', homeController.getRoot);
router.post('/', 
  [
    check('name')
      .isLength({ min: 1 })
      .withMessage('Please enter a name'),
    check('email')
      .isLength({ min: 1 })
      .withMessage('Please enter an email'),
],  homeController.postRoot );

router.get('/registrations/', registrationController.get);

module.exports = router;